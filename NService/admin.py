from django.contrib import admin
from .models import MailService, Client, Message

admin.site.register(MailService)
admin.site.register(Client)
admin.site.register(Message)
