
from django.urls import path, re_path
from .views import mod_1

urlpatterns = [
    path('', mod_1, ),
    re_path(
        r'^aliases/$',
        mod_1,
        name="m_1"
    ),
]
