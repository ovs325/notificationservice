from django.db import models


class MailService(models.Model):

    id = models.AutoField(primary_key=True)
    on_time = models.DateTimeField()
    off_time = models.DateTimeField()
    msg_text = models.TextField(null=True, blank=True, default="Sorry")
    filter_id = models.TextField(null=False, blank=True, default='{"all" = True}')
    filter_tel = models.TextField(null=False, blank=True, default='{"all" = True}')
    filter_op = models.TextField(null=False, blank=True, default='{"all" = True}')
    filter_tag = models.TextField(null=False, blank=True, default='{"tags" = Null}')
    filter_zone = models.TextField(null=False, blank=True, default='{"zone" = "МСК"}')


class Client(models.Model):

    id = models.AutoField(primary_key=True)
    telephone = models.IntegerField(null=False, blank=True, default=70000000000)
    operator = models.CharField(null=False, blank=True, max_length=15, default='MTS')
    tag = models.CharField(null=False, blank=True, max_length=150, default='Null')
    zone = models.CharField(null=False, blank=True, max_length=7, default='МСК')


class Message(models.Model):

    id = models.AutoField(primary_key=True)
    start_time = models.DateTimeField()
    status = models.CharField(null=False, blank=True, max_length=15, default='Created')
    id_mservice = models.ForeignKey(MailService, on_delete=models.CASCADE)
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
