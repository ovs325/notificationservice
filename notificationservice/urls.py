from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('_/', include('NService.urls')),
    path('admin/', admin.site.urls),
]
